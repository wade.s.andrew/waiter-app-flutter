import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './MenuItem.dart';

class NewOrder extends StatefulWidget {
  NewOrder({this.table});
  int table;
  final fs = Firestore.instance;
  State<StatefulWidget> createState() => new _NewOrderState();
}

class _NewOrderState extends State<NewOrder> {
  final List<MenuItem> menu = [];
  int count = 0;

  TextEditingController textFieldNotes = new TextEditingController();

  //Methods
  // Add an item to the orders list
  void AddItem(int table, MenuItem meal, String notes) async {
    print("here");
    await widget.fs
        .collection("Orders")
        .document('${table}')
        .collection('Order')
        .add({
      'name': meal.name,
      'price': meal.price,
      'notes': notes,
      'status': 'Draft',
      'table': table,
    });

    await widget.fs
        .collection('Tables')
        .document('$table')
        .updateData({'used': true});
    print("Adding ${meal.toString()} to table $table");
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: new AppBar(
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Menu"),
                Visibility(
                  visible: true,
                  child: Text(
                    'Table ${widget.table}',
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ],
            ),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.fastfood)),
                Tab(icon: Icon(Icons.free_breakfast)),
              ],
            ),
          ),
          body: TabBarView(children: [
            Stack(
              children: <Widget>[
                menuList(),
                Positioned(
                  bottom: 10,
                  right: 20,
                  child: FloatingActionButton.extended(
                    label: new Text(
                      "Done",
                      style: TextStyle(color: Colors.black),
                    ),
                    backgroundColor: Colors.white70,
                    onPressed: () => Navigator.pop(
                        context), /////////////////////////////////////////////////
                  ),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                drinksList(),
                Positioned(
                  bottom: 10,
                  right: 20,
                  child: FloatingActionButton.extended(
                    label: new Text(
                      "Done",
                      style: TextStyle(color: Colors.black),
                    ),
                    backgroundColor: Colors.white70,
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }

  Widget drinksList() {
    return StreamBuilder(
      stream: Firestore.instance.collection('DrinksMenu').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return const Text("Loading...");
        return _buildDrinksList(context, snapshot.data.documents);
      },
    );
  }

  Widget menuList() {
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('MealsMenu').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Text("Loading...");
          return _buildMealList(context, snapshot.data.documents);
        });
  }

  Widget _buildDrinksList(
      BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children:
          snapshot.map((data) => _buildDrinksListItem(context, data)).toList(),
    );
  }

  Widget _buildMealList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children:
          snapshot.map((data) => _buildMealListItem(context, data)).toList(),
    );
  }

  Widget _buildDrinksListItem(BuildContext context, DocumentSnapshot data) {
    final item = MenuItem.fromSnapshot(data);
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            child: Icon(Icons.add, color: Colors.black),
          ),
          title: Row(
            children: [
              Expanded(
                child: Text(
                  item.name,
                  style: Theme.of(context).textTheme.headline,
                ),
              ),
            ],
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => _drinksPopUpBuilder(context, item),
          ),
        ),
      ),
    );
  }

  Widget _drinksPopUpBuilder(BuildContext context, MenuItem drink) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                drink.name,
                style: localTheme.textTheme.display1,
              ),
              Text(
                'R ${drink.price}',
                style: localTheme.textTheme.subhead,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: textFieldNotes,
                decoration: InputDecoration(
                  hintText: 'Notes...',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text('Add'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    AddItem(widget.table, drink, textFieldNotes.text);
                    Navigator.pop(context);
                    textFieldNotes.clear();
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget _mealPopUpBuilder(BuildContext context, MenuItem meal) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                meal.name,
                style: localTheme.textTheme.display1,
              ),
              Text(
                'R ${meal.price}',
                style: localTheme.textTheme.subhead,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: textFieldNotes,
                decoration: InputDecoration(
                  hintText: 'Notes...',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text('Add'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    AddItem(widget.table, meal, textFieldNotes.text);
                    Navigator.pop(context);
                    textFieldNotes.clear();
                  }),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildMealListItem(BuildContext context, DocumentSnapshot data) {
    final item = MenuItem.fromSnapshot(data);
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            child: Icon(Icons.add, color: Colors.black),
          ),
          title: Row(
            children: [
              Expanded(
                child: Text(
                  item.name,
                  style: Theme.of(context).textTheme.headline,
                ),
              ),
            ],
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => _mealPopUpBuilder(context, item),
          ),
        ),
      ),
    );
  }
}
