import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './Order.dart';
import './Router.dart';
import './BaseAuth.dart';

class Kitchen extends StatefulWidget {
  Kitchen({this.table});
  int table;
  final fs = Firestore.instance;
  State<StatefulWidget> createState() => new _KitchenState();
}

class _KitchenState extends State<Kitchen> {
  TextEditingController textFieldNotes = new TextEditingController();

  //Methods
  updateStatus(String itemId, int table, String status) async {
    print("updating status here");
    await widget.fs
        .collection('Orders')
        .document('$table')
        .collection('Order')
        .document('$itemId')
        .updateData({'status': status});
  }

  orderCooking(String itemID) async {
    print('in start cooking');

    var item = await widget.fs
        .collection('Kitchen')
        .document('Waiting Orders')
        .collection('Order')
        .document('$itemID')
        .get();
    addToCookingList(item);
    updateStatus(itemID, item.data['table'], "In Progress");
  }

  orderComplete(String itemID) async {
    print('on order complete');

    var item = await widget.fs
        .collection('Kitchen')
        .document('In Progress Orders')
        .collection('Order')
        .document('$itemID')
        .get();
    addToCompleteList(item);
    updateStatus(itemID, item.data['table'], "Complete");
  }

  addToCookingList(var item) async {
    print('Adding to cooking');
    item.data['status'] = 'In Progress';
    await widget.fs
        .collection('Kitchen')
        .document('In Progress Orders')
        .collection('Order')
        .document('${item.documentID}')
        .setData(item.data);

    removeFromList(item.documentID, 'Waiting Orders');
  }

  addToCompleteList(var item) async {
    print('Adding to cooking');
    item.data['status'] = 'Complete';
    await widget.fs
        .collection('Complete')
        .document('${item.documentID}')
        .setData(item.data);

    removeFromList(item.documentID, 'In Progress Orders');
  }

  removeFromList(String itemID, String doc) async {
    print('deleting nfrom waiting');
    await widget.fs
        .collection('Kitchen')
        .document(doc)
        .collection('Order')
        .document('$itemID')
        .delete();
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: new AppBar(
            title: TabBar(
              tabs: [
                Tab(icon: Text('Waiting')),
                Tab(icon: Text('Cooking')),
              ],
            ),
          ),
          body: TabBarView(children: [
            StreamBuilder(
              stream: Firestore.instance
                  .collection('Kitchen')
                  .document('Waiting Orders')
                  .collection('Order')
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return const Text("Loading...");
                return _waitingKitchenList(context, snapshot.data.documents);
              },
            ),
            StreamBuilder(
              stream: Firestore.instance
                  .collection('Kitchen')
                  .document('In Progress Orders')
                  .collection('Order')
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return const Text("Loading...");
                return _cookingKitchenList(context, snapshot.data.documents);
              },
            ),
          ]),
        ));
  }

  Widget _sentToKitchenPopup() {
    ThemeData localTheme = Theme.of(context);
    return AlertDialog(
      contentPadding: EdgeInsets.all(10.0),
      content: SizedBox(
        height: 80.0,
        child: Column(children: <Widget>[
          Text(
            "Order sent to the kitchen",
            style: TextStyle(fontSize: 18.0),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10.0),
          FlatButton(
            child: Text("OK"),
            onPressed: () {
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return new Router(
                  auth: new Auth(),
                );
              }));
            },
          ),
        ]),
      ),
    );
  }

  Widget _waitingKitchenList(
      BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot
          .map((data) => _waitingKitchenListItem(context, data))
          .toList(),
    );
  }

  Widget _cookingKitchenList(
      BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot
          .map((data) => _cookingKitchenListItem(context, data))
          .toList(),
    );
  }

  Widget _waitingKitchenListItem(BuildContext context, DocumentSnapshot data) {
    final order = Order.fromSnapshot(data);
    Color statusColor;
    String itemID = data.documentID;
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                order.name,
                style: Theme.of(context).textTheme.title,
              ),
              Spacer(flex: 2),
              Text(
                order.status,
                style: TextStyle(color: GetStatusColor(order.status)),
              ),
              Spacer(flex: 2),
              FlatButton(
                padding: EdgeInsets.all(0.0),
                color: Colors.green,
                textColor: Colors.black,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Colors.blueAccent,
                child: new Text("Cook"),
                onPressed: () {
                  orderCooking(itemID);
                },
              ),
            ],
          ),
          subtitle: new Text(
            order.notes,
            overflow: TextOverflow.fade,
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => _kitchenPopUpBuilder(context, order, itemID),
          ),
        ),
      ),
    );
  }

  Widget _cookingKitchenListItem(BuildContext context, DocumentSnapshot data) {
    final order = Order.fromSnapshot(data);
    Color statusColor;
    String itemID = data.documentID;
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                order.name,
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(width: 30),
              Text(
                order.status,
                style: TextStyle(color: GetStatusColor(order.status)),
              ),
              Spacer(flex: 2),
              FlatButton(
                padding: EdgeInsets.all(0.0),
                color: Colors.green,
                textColor: Colors.black,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Colors.blueAccent,
                child: new Text("Complete"),
                onPressed: () {
                  orderComplete(itemID);
                },
              ),
            ],
          ),
          subtitle: new Text(
            order.notes,
            overflow: TextOverflow.fade,
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => _kitchenPopUpBuilder(context, order, itemID),
          ),
        ),
      ),
    );
  }

  Color GetStatusColor(String status) {
    if (status == 'Draft') {
      return Colors.yellow;
    } else if (status == 'Placed') {
      return Colors.orange;
    } else if (status == 'In Progress') {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }

  Widget _kitchenPopUpBuilder(
      BuildContext context, Order order, String itemID) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                order.name,
                style: localTheme.textTheme.display1,
              ),
              Text(
                'R ${order.price}',
                style: localTheme.textTheme.subhead,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: textFieldNotes,
                decoration: InputDecoration(
                  hintText: '${order.notes}',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text(
                    'Update',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {}),
              SizedBox(height: 10.0),
              FloatingActionButton(
                  backgroundColor: Colors.red,
                  child: new Text(
                    'Remove',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    orderCooking(itemID);
                    Navigator.pop(context);
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
