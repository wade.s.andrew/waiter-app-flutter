import 'package:cloud_firestore/cloud_firestore.dart';

class MenuItem {
  String name;
  int price;

  MenuItem(this.name, this.price);

  MenuItem.fromMap(Map<String, dynamic> map)
      : assert(map['name'] != null),
        assert(map['price'] != null),
        name = map['name'],
        price = map['price'];

  MenuItem.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data);

  @override
  String toString() {
    return '{${this.name}, ${this.price}}';
  }
}
