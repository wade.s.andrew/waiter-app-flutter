import 'package:flutter/material.dart';
import 'package:waiter_app/ReviewOrder.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './SlipPage.dart';

class Tables extends StatefulWidget {
  Tables({Key key}) : super(key: key);

  @override
  _TablesState createState() => _TablesState();
}

class _TablesState extends State<Tables> {
  int price = 0;

  //Methods
  // Sets price of the tables orders
  getPrice(int table) async {
    price = 0;
    QuerySnapshot querySnapshot = await Firestore.instance
        .collection('Orders')
        .document('$table')
        .collection('Order')
        .getDocuments();
    var list = querySnapshot.documents;

    for (var doc in list) {
      price += doc.data['price'];
    }
    print(price);
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      // appBar: AppBar(title: Text('Tables'),),
      body: StreamBuilder(
          stream: Firestore.instance.collection('Tables').snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Text('Loading...',
                  style: TextStyle(color: Colors.white));
            } else {
              return _buildGrid(context, snapshot.data.documents);
            }
          }),
    );
  }

  Widget _buildGrid(BuildContext context, List<DocumentSnapshot> snapshot) {
    return GridView.count(
        crossAxisCount: 3,
        children:
            snapshot.map((data) => _buildGridItem(context, data)).toList());
  }

  Widget _buildGridItem(BuildContext context, DocumentSnapshot data) {
    Color tableColor;
    int number = data['no'];
    if (data['used']) {
      tableColor = Colors.green;
    } else {
      tableColor = Colors.grey;
    }
    return Card(
      // color: Colors.lightGreen,
      color: tableColor,
      child: ListTile(
          title: Center(
            child: Text(
              'Table ${number}',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          onTap: () {
            showDialog(
              context: context,
              builder: (context) => _showOptions(context, number),
            );
          }),
    );
  }

  Widget _showOptions(BuildContext context, int table) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: Text(
                  "Table ${table}",
                  style: localTheme.textTheme.display1,
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                child: new Text('Order'),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return new ReviewOrder(
                      table: table,
                    );
                  }));
                },
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text('Checkout'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    getPrice(table);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return new SlipPage(
                        price: price,
                        table: table,
                      );
                    }));
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
