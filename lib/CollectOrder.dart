import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './Order.dart';

class CollectOrder extends StatefulWidget {
  final fs = Firestore.instance;
  State<StatefulWidget> createState() => new _CollectOrderState();
}

class _CollectOrderState extends State<CollectOrder> {
  TextEditingController textFieldNotes = new TextEditingController();

  //Methods
  // Update the status in the Orders list
  updateStatus(String itemId, int table, String status) async {
    await widget.fs
        .collection('Orders')
        .document('$table')
        .collection('Order')
        .document('$itemId')
        .updateData({'status': status});
  }

  // Get ID of item to be removed from collected
  orderCollected(String itemID) async {
    var item = await widget.fs.collection('Complete').document('$itemID').get();
    removeCollected(itemID);
    updateStatus(itemID, item.data['table'], "Delivered");
  }

  // Remove collected item from collect list
  removeCollected(String itemID) async {
    await widget.fs.collection('Complete').document('$itemID').delete();
  }

  // Return colour depending on status
  Color getStatusColour(String status) {
    if (status == 'Draft') {
      return Colors.yellow;
    } else if (status == 'Placed') {
      return Colors.orange;
    } else if (status == 'In Progress') {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: StreamBuilder(
        stream: Firestore.instance.collection('Complete').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Text("Loading...");
          return _completeList(context, snapshot.data.documents);
        },
      ),
    );
  }

  Widget _completeList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children:
          snapshot.map((data) => _completeListItem(context, data)).toList(),
    );
  }

  Widget _completeListItem(BuildContext context, DocumentSnapshot data) {
    final order = Order.fromSnapshot(data);
    Color statusColor;
    String itemID = data.documentID;
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                '${order.table}.',
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(width: 10),
              Text(
                order.name,
                style: Theme.of(context).textTheme.title,
              ),
              Spacer(flex: 2),
              Text(
                order.status,
                style: TextStyle(color: getStatusColour(order.status)),
              ),
              Spacer(flex: 2),
              FlatButton(
                padding: EdgeInsets.all(0.0),
                color: Colors.black54,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                splashColor: Colors.blueAccent,
                child: new Text("Collect"),
                onPressed: () {
                  orderCollected(itemID);
                },
              ),
            ],
          ),
          subtitle: new Text(
            order.notes,
            overflow: TextOverflow.fade,
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => _kitchenPopUpBuilder(context, order, itemID),
          ),
        ),
      ),
    );
  }

  Widget _kitchenPopUpBuilder(
      BuildContext context, Order order, String itemID) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                order.name,
                style: localTheme.textTheme.headline,
              ),
              Text(
                'R ${order.price}',
                style: localTheme.textTheme.subhead,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: textFieldNotes,
                decoration: InputDecoration(
                  hintText: '${order.notes}',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text(
                    'Update',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {}),
              SizedBox(height: 10.0),
              FloatingActionButton(
                  backgroundColor: Colors.red,
                  child: new Text(
                    'Remove',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
