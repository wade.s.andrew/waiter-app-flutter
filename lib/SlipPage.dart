import 'package:flutter/material.dart';
import './Order.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './Router.dart';
import 'BaseAuth.dart';

class SlipPage extends StatelessWidget {
  SlipPage({this.price, this.table});
  int price;
  int table;

  //Methods
  // Clears the bill and removes order from the table
  _clearBill(int table) async {
    print("clearing bill");
    QuerySnapshot querySnapshot = await Firestore.instance
        .collection('Orders')
        .document('$table')
        .collection('Order')
        .getDocuments();
    var list = querySnapshot.documents;

    for (var doc in list) {
      var itemID = doc.documentID;
      await Firestore.instance
          .collection('Orders')
          .document('$table')
          .collection('Order')
          .document('$itemID')
          .delete();
    }
  }

  //Set table to not being used
  endUseTable(int table) async {
    await Firestore.instance
        .collection('Tables')
        .document('$table')
        .updateData({'used': false});
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: new AppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Bill"),
              Visibility(
                visible: true,
                child: Text(
                  'Table ${table} order',
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: StreamBuilder(
          stream: Firestore.instance
              .collection('Orders')
              .document('$table')
              .collection('Order')
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Text(
                "Loading...",
                style: TextStyle(color: Colors.white),
              );
            } else {
              return _buildBill(context, snapshot.data.documents);
            }
          },
        ),
        bottomNavigationBar: Card(
          elevation: 0.0,
          margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "Total: ",
                    style: Theme.of(context).textTheme.headline,
                  ),
                  Text(
                    '${price}',
                    style: Theme.of(context).textTheme.headline,
                  ),
                  FloatingActionButton.extended(
                    focusColor: Colors.blueGrey,
                    label: new Text(
                      "Finish",
                      style: TextStyle(color: Colors.black),
                    ),
                    backgroundColor: Colors.white70,
                    onPressed: () {
                      endUseTable(table);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return new Router(
                          auth: new Auth(),
                        );
                      }));
                      _clearBill(table);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBill(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((data) => _buildBillItem(context, data)).toList(),
    );
  }

  Widget _buildBillItem(BuildContext context, DocumentSnapshot data) {
    final order = Order.fromSnapshot(data);
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: ListTile(
          trailing: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.edit, color: Colors.black),
          ),
          title: Row(
            children: [
              Text(
                order.name,
                style: Theme.of(context).textTheme.headline,
              ),
              SizedBox(width: 30),
              Text(
                '${order.price}',
              ),
            ],
          ),
          subtitle: new Text(order.notes),
        ),
      ),
    );
  }
}
