import 'package:flutter/material.dart';
import './CollectOrder.dart';
import './Tables.dart';
import './BaseAuth.dart';
import './Router.dart';
import 'Kitchen.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedPage = 0;
  final _pages = [
    Tables(),
    Kitchen(),
    CollectOrder(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Waiter App'),
        actions: <Widget>[
          FlatButton(
              child: new Text("Logout"),
              onPressed: () {
                widget.auth.signOut();
                widget.logoutCallback();
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return new Router(
                    auth: new Auth(),
                  );
                }));
              }),
        ],
      ),
      body: _pages[_selectedPage],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueGrey,
        fixedColor: Colors.black,
        currentIndex: _selectedPage,
        onTap: (int index) {
          setState(() {
            _selectedPage = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.apps),
            title: Text('Select Table'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('Kitchen'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.arrow_upward),
            title: Text('Collect'),
          ),
        ],
      ),
    );
  }
}
