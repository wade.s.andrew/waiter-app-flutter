import 'package:flutter/material.dart';
import './BaseAuth.dart';
import './Router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Flutter login demo',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: new Router(auth: new Auth()));
  }
}
