import 'package:cloud_firestore/cloud_firestore.dart';

class Order {
  String name, notes, status;
  int price, table;

  Order(this.name, this.price, this.notes, this.status, this.table);

  Order.fromMap(Map<String, dynamic> map)
      : assert(map['name'] != null),
        assert(map['price'] != null),
        assert(map['notes'] != null),
        assert(map['status'] != null),
        // assert(map['table'] != null),
        name = map['name'],
        price = map['price'],
        status = map['status'],
        table = map['table'],
        notes = map['notes'];

  Order.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data);

  @override
  String toString() {
    return '{${this.name}, ${this.price}, ${this.notes}, ${this.status}}';
  }
}
