import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:waiter_app/NewOrder.dart';
import './MenuItem.dart';
import './Order.dart';
import './Router.dart';
import './BaseAuth.dart';

class ReviewOrder extends StatefulWidget {
  ReviewOrder({this.table});
  int table;
  final fs = Firestore.instance;
  State<StatefulWidget> createState() => new _ReviewOrderState();
}

class _ReviewOrderState extends State<ReviewOrder> {
  TextEditingController textFieldNotes = new TextEditingController();
  bool _buttonDisabled = true;

  //Methods
  // Send orders to the kitchen list
  void sendOrderToKitchen(int table) async {
    print("Sending to kitchen");
    QuerySnapshot querySnapshot = await widget.fs
        .collection('Orders')
        .document('$table')
        .collection('Order')
        .getDocuments();
    var list = querySnapshot.documents;

    for (var doc in list) {
      if (doc.data['status'] == 'Draft') {
        var newDoc = doc.data;
        newDoc['status'] = 'Placed';
        newDoc['timeIn'] = new DateTime.now().millisecondsSinceEpoch;
        print('doing it');
        await widget.fs
            .collection('Orders')
            .document('$table')
            .collection('Order')
            .document('${doc.documentID}')
            .updateData({'status': 'Placed'});
        await widget.fs
            .collection('Kitchen')
            .document('Waiting Orders')
            .collection('Order')
            .document('${doc.documentID}')
            .setData(newDoc);
      }
    }
    // OrderPlacedStatus(table);
  }

  // Updates an items status in the Orders list
  void updateItem(int table, MenuItem meal, String notes, String itemID) async {
    print("Updating item");
    await widget.fs
        .collection("Orders")
        .document('$table')
        .collection('Order')
        .document('$itemID')
        .updateData({
      'name': meal.name,
      'price': meal.price,
      'notes': notes,
      'status': 'Draft',
      'table': table,
    });
    print("Adding ${meal.toString()} to table $table");
  }

  // Removes an item from the Orders list
  void removeItem(int table, String itemID) async {
    print("removing item");
    await widget.fs
        .collection("Orders")
        .document('${table}')
        .collection('Order')
        .document('$itemID')
        .delete();
    print("Deleted Meal from Table $table");
  }

  // Returns a Meal object from an Order object
  MenuItem mealFromOrder(Order order) {
    MenuItem item = new MenuItem(order.name, order.price);
    return item;
  }

  // Sets status of an item to 'In Progress'
  void cookOrderStatus(int table) async {
    var batch = widget.fs.batch();
    const newState = {
      'status': "InProgress",
    };
    print("changing status to in progress");
    await widget.fs
        .collection('draft')
        .document('${table}')
        .collection('order')
        .where('status')
        .getDocuments()
        .then((response) {
      response.documents.forEach((doc) {
        var docRef = widget.fs
            .collection('draft')
            .document('$table')
            .collection('order')
            .document(doc.documentID);
        batch.updateData(docRef, newState);
      });
      batch.commit();
    });
  }

  // Sets status of an item to 'Complete'
  void orderReadyStatus(int table) async {
    var batch = widget.fs.batch();
    const newState = {
      'status': "Completed",
    };
    print("chzanging stataus to completed");
    await widget.fs
        .collection('draft')
        .document('${table}')
        .collection('order')
        .where('status')
        .getDocuments()
        .then((response) {
      response.documents.forEach((doc) {
        var docRef = widget.fs
            .collection('draft')
            .document('$table')
            .collection('order')
            .document(doc.documentID);
        batch.updateData(docRef, newState);
      });
      batch.commit();
    });
  }

  // Sets Colour depending on status
  Color getStatusColour(String status) {
    if (status == 'Draft') {
      return Colors.yellow;
    } else if (status == 'Placed') {
      return Colors.orange;
    } else if (status == 'In Progress') {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }

  //Design
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: Stack(
        children: <Widget>[
          tableOrderList(
            context,
            widget.table,
          ),
          Positioned(
            bottom: 60,
            right: 20,
            child: FloatingActionButton.extended(
              focusColor: Colors.blueGrey,
              icon: Icon(
                Icons.add,
                color: Colors.black,
              ),
              label: new Text(
                "Add Item",
                style: TextStyle(color: Colors.black),
              ),
              backgroundColor: Colors.white70,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return new NewOrder(
                    table: widget.table,
                  );
                }));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget tableOrderList(BuildContext context, int table) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Menu"),
            Visibility(
              visible: true,
              child: Text(
                'Table ${widget.table} order',
                style: TextStyle(
                  fontSize: 15.0,
                ),
              ),
            ),
          ],
        ),
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .collection('Orders')
            .document('$table')
            .collection('Order')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            _buttonDisabled = true;
            return const Text(
              "Loading...",
              style: TextStyle(color: Colors.white),
            );
          } else {
            _buttonDisabled = false;
            return _buildOrderList(context, snapshot.data.documents);
          }
        },
      ),
      bottomNavigationBar: BottomAppBar(
        color: _buttonDisabled ? Colors.grey : Colors.green,
        child: FlatButton(
          padding: EdgeInsets.all(0.0),
          color: _buttonDisabled ? Colors.grey : Colors.green,
          textColor: Colors.black,
          disabledColor: Colors.grey,
          disabledTextColor: Colors.black,
          // padding: EdgeInsets.all(8.0),
          splashColor: Colors.blueAccent,
          child: new Text(
            'Place Orders',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed: () {
            _buttonDisabled ? null : sendOrderToKitchen(table);
          },
        ),
      ),
    );
  }

  Widget _buildOrderList(
      BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children:
          snapshot.map((data) => _buildOrderListItem(context, data)).toList(),
    );
  }

  Widget _buildOrderListItem(BuildContext context, DocumentSnapshot data) {
    final order = Order.fromSnapshot(data);
    Color statusColor;
    String itemID = data.documentID;
    if (order.status == 'Draft') {
      return Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            title: Row(
              children: [
                Text(
                  order.name,
                  style: Theme.of(context).textTheme.title,
                ),
                Spacer(flex: 2),
                Text(
                  order.status,
                  style: TextStyle(color: getStatusColour(order.status)),
                ),
                Spacer(flex: 2),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(Icons.edit, color: Colors.black),
                ),
              ],
            ),
            subtitle: new Text(order.notes),
            onTap: () => showDialog(
              context: context,
              builder: (context) => _orderPopUpBuilder(context, order, itemID),
            ),
          ),
        ),
      );
    } else if (order.status == 'InProgress' || order.status == 'Placed') {
      return Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            trailing: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircularProgressIndicator(),
            ),
            title: Row(
              children: [
                Text(
                  order.name,
                  style: Theme.of(context).textTheme.headline,
                ),
                SizedBox(width: 30),
                Text(
                  order.status,
                  style: TextStyle(color: getStatusColour(order.status)),
                ),
              ],
            ),
            subtitle: new Text(order.notes),
          ),
        ),
      );
    } else {
      return Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            trailing: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.arrow_upward),
            ),
            title: Row(
              children: [
                Text(
                  order.name,
                  style: Theme.of(context).textTheme.headline,
                ),
                SizedBox(width: 30),
                Text(
                  order.status,
                  style: TextStyle(color: getStatusColour(order.status)),
                ),
              ],
            ),
            subtitle: new Text(order.notes),
          ),
        ),
      );
    }
  }

  Widget _orderPopUpBuilder(BuildContext context, Order order, String itemID) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                order.name,
                style: localTheme.textTheme.display1,
              ),
              Text(
                'R ${order.price}',
                style: localTheme.textTheme.subhead,
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: textFieldNotes,
                decoration: InputDecoration(
                  hintText: '${order.notes}',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              FloatingActionButton(
                  child: new Text(
                    'Update',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    updateItem(widget.table, mealFromOrder(order),
                        textFieldNotes.text, itemID);
                    textFieldNotes.clear();
                    Navigator.pop(context);
                  }),
              SizedBox(height: 10.0),
              FloatingActionButton(
                  backgroundColor: Colors.red,
                  child: new Text(
                    'Remove',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  onPressed: () {
                    removeItem(widget.table, itemID);
                    Navigator.pop(context);
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
